---
title: "About"
date: 2018-07-04T13:11:05+05:30
draft: false
---
Hey,

My name is Arjun and I'm a software developer in Kochi, India.

Professionally I work mostly as a web developer doing PHP and Laravel, but I have knowledge of other areas of the tech-stack as well. I sometimes contribute to open-source projects, such as Mozilla Firefox.

Recently I've been experimenting with functional programming languages such as Haskell, StandardML, and Elm. Because it requires a different mindset than what I'm used to, I find the learning experience a refreshing challenge.

This is my mostly-technical blog where I maintain notes of things I tinker with. Feel free to poke about. I do have plans to diversify the contents here. The theme of this website was also developed by me; check out the details of that from [here.](https://github.com/arjkb/basics)

I have an MS in Computer Science from the University of New Mexico, USA.

Thanks!