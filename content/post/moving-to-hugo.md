---
title: "Moving to Hugo"
date: 2018-07-05T15:33:29+05:30
draft: false
---
I hereby announce that I have moved from [Jekyll](https://jekyllrb.com/) to [Hugo.](https://gohugo.io/)

I originally moved to Jekyll from Wordpress in the summer of 2015 because static sites made sense for my specific use-case. Also, WordPress provided more features than what I need, and because I wasn't using a self-hosted version, I felt like I couldn't get the ultimate control that I wanted.

However, to take full advantage of Jekyll and customize it the way I want, I had to know ruby -- a language I wasn't keen on learning; it's a great language, but I'd rather spend my time learning haskell or lisp rather than ruby.

Nevertheless, Jekyll was infinitely better than Wordpress. I was using a theme somebody else wrote, was happy with what the site offered me, and was happy with the way it looked on both large and small screens. However, there were some niggling issues:

* [Setting up the local development environment](https://help.github.com/articles/setting-up-your-github-pages-site-locally-with-jekyll/) is a complicated procedure.
* The site takes forever to build. Like, 6 seconds on my machine! And I don't even have a lot of posts!
* The theme I used was great for what it was -- gorgeous and worked well on all screen sizes. But it was way more than what I personally wanted. I was looking for something clean and simple.
* Because I didn't know ruby (and wasn't willing to learn), rolling out my own theme seemed like a bad idea.

I had to move to a different platform. My first pick was [Pelican](https://blog.getpelican.com/). It was written in Python -- a language I'm comfortable with. However, I decided to give Hugo a go (pun intended). I'm familiar with Go (using which Hugo is built), and Hugo claimed to be "the world's fastest framework for building websites". That was tempting.

I dived right in. Here's what I like about Hugo:

* Setting up a fully-functional website -- including installation of necessary packages -- took less than 3 minutes.
* Lighting-fast build times.
* Installing a theme is a one-liner. Cannot possibly get easier.

There are some downsides however. The documentation could be improved. Right now it tells you the "what" of everything, but does a bad job explaining the "how". Thankfully, there's a [video tutorial from Giraffe Academy](https://www.giraffeacademy.com/static-site-generators/hugo/) that helped a lot. Some of those videos are embedded in the [official Hugo docs](https://gohugo.io/documentation/) and are available on YouTube.

For my blog, I initially used a theme called [Elephants](https://themes.gohugo.io/elephants/) from the [official themes showcase](https://themes.gohugo.io/), but there were some aspects of it I disliked. For instance, it did not have ready support for single pages (such as an About page), top navigation bar etc. So I made my own theme that had the look and feel of Elephants, but with all the additional functionality that I wanted. ~~This theme is still a work in progress -- in particular, I'm not entirely happy with the way the theme renders on mobile screens. Once I perfect it, I'll make it available on the official theme showcase.~~ It feels great to contribute back to the community.

**EDIT:** The theme I have built is called [Basics](https://themes.gohugo.io/basics/), and is now available on the [official Hugo themes](https://themes.gohugo.io/) site for you to use on your Hugo website!

## Migration

Since both Hugo and Jekyll posts were written in markdown with a similar front matter, all I had to do was copy over the `.md` post files. However, the post dates were screwed up; in Hugo you specify the date in the front matter, whereas in Jekyll the date is part of the `.md` filename itself. For example, a post titled "Hello World" published on Jan 15 2006 would be named as `2006-01-15-hello-world.md` in Jekyll.

None of my post files from Jekyll has the date in the front-matter, and I wasn't going to manually add in this information to all my post files. So I wrote the following script to do this for me:

<script src="https://gist.github.com/arjunkrishnababu96/0b922ca0f7cd39a017cab4735fd3e0b4.js"></script>

The script grabs the date from the filename and adds it to the line right after the "title:" line in the front matter. Perhaps not the most elegant way to do things, but it worked well for me. Feel free to use it in whatever way you want to.

And, unlike my earlier blog which was hosted on [GitHub Pages](https://pages.github.com/), I've hosted this new one on [GitLab Pages](https://about.gitlab.com/features/pages/) instead. It has ready support for Hugo, and this switch is part of a larger plan for me to transition to GitLab for nearly all my source-control activities.
