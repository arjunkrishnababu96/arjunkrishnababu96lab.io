---
layout: post
title: Introducing JekPost
date: 2017-08-14
excerpt: Script to make creation of Jekyll posts trivial.
tags: jekyll, python
---
As much as I love [jekyll](https://jekyllrb.com/), one thing I found really annoying about it was the post creation ritual itself. You had to name the file a certain way and type in a bunch of metadata at its top for the whole thing to work. Cumbersome and repetitive.

That's when I decided to write a script to simplify the post creation process.

It's actually been a year since I wrote JekPost. It was something I wrote for myself, and had not really thought about sharing it with anybody. There were some minor nuances, but they were things I could live with. After a while I decided that I might as well upload the package to [pypi](https://pypi.python.org/pypi), which in itself was a learning experience.

Recently, I fixed the aforementioned nuances. At the time of writing, JekPost is at version 2.1.0, and is very polished for what it does:
* Create a new jekyll post file with appropriately formatted filename.
* Add a basic YAML front matter block to the top of the post file.

JekPost is written in python. You need python 3 to run it.

To install jekpost run  `pip install jekpost`. Restart your terminal after installation.

To create a new post, run:
```
$ jekpost 'My Awesome Post' dir
```

* `My Awesome Post` is the post title.
* `dir` is where you want the post file to be; this is typically the `_posts` directory.
* `2017-08-14-my-awesome-post.md`* would be created inside `dir` with the necessary YAML front matter.

There! That takes care of all the heavy lifting associated with creating a new jekyll post.

Let me run though a full example:
```
$ jekpost 'My Awesome Post' _posts
Post Title: My Awesome Post
New post created: 2017-08-14-my-awesome-post.md
$ cat _posts/2017-08-14-my-awesome-post.md
---
layout: post
title: My Awesome Post
date: 2017-08-14
excerpt: Your excerpt goes here
tags: tag1, tag2
---
Your post content goes here
```

You should replace the excerpt, tags, and post content with values specific to your post.

If you use jekyll, try out JekPost. I could use some feedback!

#### Project Links
* JekPost on GitHub: [https://github.com/arjunkrishnababu96/jekpost](https://github.com/arjunkrishnababu96/jekpost)
* JekPost on PyPi: [https://pypi.python.org/pypi/JekPost](https://pypi.python.org/pypi/JekPost)