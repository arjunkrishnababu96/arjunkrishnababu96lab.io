---
title: "How to setup OpenProject via Vagrant"
date: 2021-02-15T08:33:22+05:30
draft: false
---
I had to set up OpenProject locally on my machine for certain tinkering I've been doing. 

I chose to install OpenProject inside a Vagrant box to keep my native machine clean. Went along with [hashicorp's Ubuntu  18.04 LTS](https://app.vagrantup.com/hashicorp/boxes/bionic64) box rather than an official release because the former was already in my machine which meant I did not have to do any additional downloads.

Installation and configuration of OpenProject was straightforward thanks to their excellent documentation [here.](https://docs.openproject.org/installation-and-operations/installation/packaged/#install-openproject-with-debrpm-packages)

Here's how my configuration looks like:
```
vagrant@vagrant:~$ sudo cat /etc/openproject/installer.dat
openproject/edition default
postgres/autoinstall install
postgres/db_host 127.0.0.1
postgres/db_port 45432
postgres/db_name openproject
postgres/db_username openproject
postgres/db_password xxxxxxxxxxxxxxxxxxxxx
server/autoinstall install
server/variant apache2

server/hostname openproject.example.com
server/server_path_prefix 
server/ssl no
repositories/api-key xxxxxxxxxxxxxxxxxxxxxxx
repositories/svn-install skip
repositories/git-install skip
smtp/autoinstall skip
memcached/autoinstall install
postgres/addon_version v1
```

Chose not to set up SSL because what I'm doing is not meant to go into production and is purely experimental.

Next, I verified that I could access OpenProjcet inside my Vagrant box by doing a `wget openproject.example.com`

Next, I had to make it accessible via the browser on my host machine; uncommented the following line which was already there in the Vagrantfile (courtesy Hashicorp's box):
```
config.vm.network "private_network", ip: "192.168.33.10"
```

Once I did that and reprovisioned my machine (via `vagrant reload --provision`), I was able to access the OpenProject homepage from my host machine by going to the IP mentioned above. To make it so that I could visit `openproject.example.com` instead of the IP, I added the following entry in my `/etc/hosts`:

```
192.168.33.10	openproject.example.com
```

It worked.