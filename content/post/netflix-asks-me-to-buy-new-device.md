---
title: "Netflix Asks Me to Buy New Device"
date: 2022-06-12T01:20:43+05:30
draft: false
---

I have been a happy customer of Netflix for over two years even though I don't even watch it enough to justify what I pay (and where I'm from Netflix is substantially more expensive than other streaming platforms). But I figured it's fine.

I usually watch Netflix via a ThinkPad running Linux, connected to an external monitor. *I'm old school.*

Yesterday I decided to watch a movie that will soon be removed from Netflix. I signed into my Linux laptop and went to [netflix.com](https://www.netflix.com/) (the page loaded fine), and from there clicked the login link.

I was greeted with the below page:

![blank Chrome page](/images/netflix-chat/chrome_blank.png)

## Troubleshooting
* `403 Forbidden` on the network tab.
* Cleared cookies and whatever else that can possibly be cleared, and retried in vain.
* Tried other browsers in vain.
* Can access Netflix app on my phone. It works. I can play stuff.
    * However, if I access Netflix via a browser on my phone, then it gives me the same blank page I see on my desktop browser.
* Turned on WiFi hotspot on my smartphone, and connected my laptop to that. It works! However, this is just a temporary solution. My usual home WiFi is where I want to connect to Netflix from.

I suspect it's something about my WiFi network that's preventing me from accessing [netflix.com/login](https://www.netflix.com/login). I didn't reconfigure anything on my network recently, and Netflix worked fine on this same laptop last week (and before that).

## Call with Customer Support
I contacted Netflix help. Within a couple of minutes I was chatting with one of their support staff. My wishful thinking was:
* They (it's just one person, but I'm trying to be gender-neutral) will take a look at things on their end, and magically solve the problem.
* If they can't solve the problem, they'll at least  tell me what's wrong with requests from my network so that I can try to fix those.
    * If it's something involving my ISP, I can try to contact my ISP and work on resolving it at their end. (If that doesn't work I can change my ISP).

They eventually ask me what device I'm on, and I tell them I'm on a ThinkPad running Linux. Then they ask if the problem is when I'm opening the app or when I try to play something. I explain that there's no app for Netflix on Linux (as far as I know) and that I always log into Netflix via the browser.

Later they ask about my network. *Okay now we're getting somewhere.* I mention that I can access Netflix on my Linux laptop when I'm connected to a different WiFi network.

After a minute **the support staff tells me that they don't really support Netflix on Linux:**

(blacked out the support staff's name from the screenshots)

![nf1](/images/netflix-chat/nf1.jpg)

Then they **tell me to use another device**, and that's when I realized this is hopeless. Here are some other noteworthy bits:

**Exhibit 1**

![nf1](/images/netflix-chat/nf2.jpg)

**Exhibit 2**

![nf1](/images/netflix-chat/nf3.jpg)

I don't know whether to laugh or cry. I'm not even angry; I'm just amused at the lack of empathy.
* It just doesn't occur to the support staff that it might be a network problem.
* The support staff never contemplates on the fact that
    * I can access Netflix on my Linux laptop from another network.
    * I was able to access Netflix on the same Linux laptop on the same network until today.

Even if I was told that the problem is my internet, it would have been okay and I would have considered other solutions (including potentially switching ISP). However, this person has managed to turn this around to blame this on Linux, and then proceeded to say  I should get a new device!

## Conclusions

I'm appalled at the lack of empathy and poor troubleshooting skills of the support staff I chatted with.

Netflix content isn't so great that I'll go buy a new device for it. (And all my current devices run Linux -- which really is besides the point here).

Just to be clear, here are some things that I want to emphasize:
* Until this point, I was a generally happy customer of Netflix.
* The one time prior to this when I did have a problem, it was quickly resolved by one of their support staff over chat.
* Of all the streaming apps that I have tried (which, frankly, isn't a lot), Netflix seems to be the best-engineered in terms of snappiness, perceived performance, data-usage etc. (Content selection is a whole different matter though).

All this is to say, I do not think Netflix as a company is horrible. However, they do have some teething issues that needs to be examined.

I'm going to wait until the end of my current billing cycle to see if the problem resolves itself. If it doesn't, I'll be unsubscribing.

---

**Update on June 18:** I was able to log into Netflix from my usual linux laptop. I definitely did not make any network changes, and I doubt if Netflix did anything on their end. Probably whatever it was from my internet provider (most likely) got resolved on its own.
