---
title: "In America"
date: 2016-01-13T19:10:18+05:30
draft: false
---

![blank Chrome page](/images/seatac_1.jpg)

Just landed at Seattle-Tacoma International Airport, in Seattle, USA.

I'll be spending my upcoming semester here in America as part of a student-exchange program; very excited! Just 5 months ago, America wasn't even in my radar, and now here I am!
