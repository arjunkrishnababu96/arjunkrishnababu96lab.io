---
layout: post
title: Desktop Entry for Matlab
date: 2018-01-16
modified:
categories:
excerpt: A procedure for creating desktop-entries.
tags: [gnome, desktop-entries, matlab]
image:
feature:
date: 2018-01-16T18:50:10-07:00
hrefTargetBlank: true
---

*This article is not about Matlab itself; I just used it as a test example to demonstrate how you can create a desktop entry in Gnome.*

*However, if you are interested in using Matlab, I highly insist you try [GNU Octave](https://www.gnu.org/software/octave/). The syntax is highly compatible with Matlab, and I was able to do on Octave everything I was supposed to do on Matlab, without any hassles whatsoever. Octave is free and open-source, and you've got nothing to loose by trying it out.*

---

After installing [Matlab](https://www.mathworks.com/products/matlab.html), I was dismayed to discover that there was no desktop-entry for it. It is sometimes annoying to open a predominantly graphical software via the terminal. So I created a desktop-entry for Matlab on my own.

All of this was done on Debian 9, using Gnome 3.22.2 desktop environment.

On my particular machine, Matlab was installed at `/usr/local/MATLAB`.  In particular, the `/usr/local/MATLAB/R2017b/bin/` directory contains executables, like the one for actually launching Matlab.

Infact, the only way to open Matlab after installation was by running `/usr/local/MATLAB/R2017b/bin/matlab -desktop` on my terminal, which is exactly what I want to avoid by creating a desktop entry for Matlab.

There are two steps to creating a desktop entry:

1. [Obtain an icon for the application.](#obtain-an-application-icon)
2. [Create a desktop-entry file and place it in an appropriate location.](#create-a-desktop-entry-file)

These steps can be tailored for creating a desktop-entry for any application that doesn't have it by default.

### Obtain an Application Icon
Theoretically, the icon can be any image file. A Matlab icon is available at [https://upload.wikimedia.org/wikipedia/commons/2/21/Matlab_Logo.png](https://upload.wikimedia.org/wikipedia/commons/2/21/Matlab_Logo.png).

Download the file and put it somewhere where the file is unlikely to get disturbed. I chose to put the file into the Matlab installation directory at `/usr/local/MATLAB`. To do this via the command line, run:

```
$ cd /usr/local/MATLAB
$ wget https://upload.wikimedia.org/wikipedia/commons/2/21/Matlab_Logo.png && mv Matlab_Logo.png logo.png
```

The second command above downloads the file into the current directory and renames it to `logo.png`.

### Create a Desktop Entry File

For a full understanding of this process, read the official Gnome documentation at  [https://developer.gnome.org/integration-guide/stable/desktop-files.html](https://developer.gnome.org/integration-guide/stable/desktop-files.html).

Create a `.desktop` file. Pick a good descriptive name -- I chose `matlab-launch.desktop`. Since I'm the only user on my computer who would use Matlab, I put the `.desktop` file in `.local/share/applications/`.

The contents of my `matlab-launch.desktop` are as follows:
```
[Desktop Entry]
Name=Matlab
Exec=/usr/local/MATLAB/R2017b/bin/matlab -desktop
Icon=/usr/local/MATLAB/logo.png
Type=Application
Encoding=UTF-8
Terminal=false
Categories=Development;Science;Education;
```

The crucial fields are:

* Exec: Program to execute; the terminal command that would open the application.
* Icon: Path to the application icon you have saved earlier.

The rest of the fields should be self-explanatory, and not all of these are required. For a detailed understanding of each of those fields, read the [official documentation.](https://developer.gnome.org/desktop-entry-spec/#recognized-keys)

Almost immediately after you save the desktop file, the Matlab icon should appear in your application menu, and clicking it should open Matlab.
