---
title: "In China, Macau, & Hong Kong"
date: 2024-05-05T22:00:23+05:30
draft: false
---

I recently returned from a sightseeing tour of China, Macau, and Hong Kong. I visited Beijing, Shanghai, Macau, and Hong Kong, in that order.

As you're probably aware, common internet things like Google (including search, GMail, Drive, Docs, Play Store, YouTube etc), WhatsApp, Facebook, Instagram, X, or anything else that you take for granted don't work in China. Most VPNs are also blocked, although people have claimed varying degrees of success with them.

Even so, it turns out that international roaming traffic gets routed through your home country. So you get a quasi-VPN when you're in international roaming due to which I was able to use the "regular" services just fine. For an extra measure, I turned off the automatic updates on my phone with the hope that the apps wouldn't realize they're in China and alter its behavior. This was probably overkill on my part. If you connect to the WiFi in hotels those aforementioned services don't work. These internet problems did not exist in Macau and Hong Kong.

The Chinese use alternates such as Baidu and WeChat. My own apprehensions about their technical merits aside, none of the people I personally know use these apps so there was no point in me signing up.

Beijing, although cleaner than my hometown in India, wasn't spotless. Some things felt old. Some buildings had fogged up windows and worn-out paint. Beijing airport itself seemed underwhelming structure-wise, yet it seems to be capable of handling a huge influx of passengers. I'm aware they had renovated the airport as preparation for the Olympics back in the day, but let's just say I've seen cosier airports. You need to fill an arrival form once you're there; it asks for information such as the hotel you're staying at, your contact in Beijing etc. I wish they gave us these forms from the plane itself; some countries/airlines do that.

There's traffic during the rush hours in the morning and evening. Yet it was far better than the traffic I experience in India. The roads are overall in a great condition.

Cost of housing, land etc is prohibitively high, but the cost of a new vehicle is low in comparison. So – as per my tour guide – people would buy houses far from the city and buy a nice car for their commutes. The guide emphasized that around here somebody's car is no indication of their wealth, but on the other hand if they have multiple apartments they could be rich.

In China, people drive on the right side of the road, and in Macau and Hong Kong, people drive on the left. Electric cars are common on the roads.

The Tiananmen Square was bustling with people.

![](/images/cmh_apr_may_2024/beijing_tiananmen.jpg)

Our local guide did not explain what actually happened there. I didn't bother asking either, as I didn't want to put her in a difficult situation.

I also attended the [Golden Mask Dynasty show](https://www.goldenmaskdynasty.cn/). It's sort of like a play that's an hour long. It was the most spectacular thing I've even seen so far! If you're ever in Beijing, I highly recommend it.

The Great Wall is more than an hour away from Beijing by road. It has so many towers throughout its length. There are walkways between the towers, and sometimes even stairs to accomodate for it going through mountains and uneven terrain. The stairs are steep and sometimes of uneven height. Our guide warned us to either do the climbing, or to take photos, but not to do both as it can be dangerous. The structure is impressive and photos cannot do justice.

![](/images/cmh_apr_may_2024/gw_stairs.jpg)

Don't believe anyone who says you can see it from the moon. [You can't.](https://www.scientificamerican.com/article/is-chinas-great-wall-visible-from-space/) It may be possible to see it in low-earth orbit if you know exactly where to look and squint, under favorable lighting conditions.

We also visited the jade factory near the Great Wall. They're nice and more expensive than I thought. See the below image. I don't know what this is, but this jade sculpture costs well over ¥6,80,000 ($93,000+, ₹78,25,000+) as per the price tag on it.

![](/images/cmh_apr_may_2024/jade_sculpture.jpg)

Cannot imagine myself ever buying the above, but I could stomach the idea of buying the beautiful vase below. Far cheaper at ¥38800 ($5300+, ₹4,46,000+). Perhaps on another trip once I'm richer!

![](/images/cmh_apr_may_2024/jade_vase.jpg)

The Beijing train station was bustling with people.
![](/images/cmh_apr_may_2024/beijing_bullet_train_station.jpg)

A high-speed train took me from Beijing to Shanghai. It covered the 1300+ km distance in 4.5 hours. That's nearly the distance between Kochi and Mumbai! My local tour guide said that back when she was a child it took days to travel from Beijing to Shanghai. The train itself was comfortable and smooth. The indicated speeds were around 340 km/h for most of the trip. There were 3 intermediate stops. The ride was rather supple.

Shanghai is extremely clean and modern. Some really tall buildings too.

![](/images/cmh_apr_may_2024/shanghai_cityscape.jpg)
The above picture was taken during a cruise through the Huangpu river.

I took a [maglev](https://en.wikipedia.org/wiki/Shanghai_maglev_train) train just for the sake of riding on one, from the airport to the city. The distance of about 35 kilometers was covered in about 7 minutes. However, I did not feel any levitation (granted, the 10 mm levitation may be hard to detect), the speed was limited to 300 km/h, the ride wasn't as comfortable as the bullet train from Beijing, and the cabin itself was underwhelming. I expected a smooth ride considering there's levitation involved. Our guide said that the top speed was over 400 km/h pre-pandemic but she doesn't know why the speed has been restricted now. The "conventional" non-magnetic high-speed train (such as the one I took to get from Beijing to Shanghai) is faster and cosier; I don't see the advantage of maglev.

There's also this market called "AP market" where you'll get cheap fake goods and you can bargain hard. The place is underground and is a maze of shops. There's bags, belts, electronics, and anything else you can think of. I strolled through the place but did not buy anything.

Public toilets are not great in China. They're usually not clean. Squat toilets are common, but other types exist and the type of the toilet is usually indicated on its door. Unfortunately, most public toilets have neither a toilet paper, nor a hose, nor a pipe, nor a bucket or anything! So if you're going to visit China, always carry a stash of toilet paper with you! This was the case in the touristy places as well, such as the ones near the Great Wall. Public toilets were far better in Macau and Hong Kong.

Something weird I noticed in China was the absence of birds. There's trees and parks everywhere in the city, but they're devoid of birds! I did notice one small bird in Beijing but that was it! Couldn't spot a single one anywhere else in Beijing, or in Shanghai. There were plenty of birds in Macau and Hong Kong.

You need to go through immigration procedures when you enter Macau and Hong Kong (separately). During immigration into Macau they'll give you a small piece of paper which you shouldn't loose! You need to submit that to the hotel when you check-in!

During this trip I went atop two TV towers, one in Shanghai, and another in Macau. The oriental pearl TV tower in Shanghai was bustling with people, and we had to wait over an hour to reach the top.

![](/images/cmh_apr_may_2024/shanghai_skyline.jpg)

The views atop were great. Some of the more taller buildings seem to rise above the clouds, although it could just be the fog.

Something unique I noticed was that the elevator in the Shanghai TV tower has two levels!

The TV tower in Macau was less exciting; only a few people were there. But unlike Shanghai this one has a bungee jumping facility! Not only that, it's one of the highest bungee jumping facilities in the world! And while we were there two people bungee jumped! It costs about $500 per person.

I strolled into [The Venetian Macao](https://en.wikipedia.org/wiki/The_Venetian_Macao). It's a famous casino resort. The upper level of the Venetian had a mall-like thing with an artificial roof that's convincing at first glance.

![](/images/cmh_apr_may_2024/venetian_artificial_sky.jpg)

Gambling was happening in full-swing. Most tables had a group of onlookers. Since I didn't know the games that they were playing, I quickly lost interest and left the place. I suppose I was expecting a scene out of [Casino Royale](https://en.wikipedia.org/wiki/Casino_Royale_(2006_film)).

There was a magic show happening, and this man had quite a few tricks up his sleeve.
![](/images/cmh_apr_may_2024/venetian_magician.jpg)

The hotel we stayed at in Hong Kong did not provide us drinking water in the room. We had to buy one from the shop downstairs. Buildings in Hong Kong have a shopping centre on the bottom floors, even if the rest of the building is something unrelated, such as a hotel or an apartment building. My Visa debit card worked fine in Hong Kong when I used it to buy bottles of water.

I asked my various tour guides (one from each city) about the nature of relationship of China with Macau and Hong Kong, and I couldn't get a straight answer from any of them.

I came across an English-language newspaper, and I regret not taking a photograph of its pages.

The Indian power cords worked in Beijing, Shanghai, and Macau, but I had to use an adapter in Hong Kong. Our hotel in Macau had charging cables provided in the room, which was just fantastic. Every hotel should do this!
![alt text](/images/cmh_apr_may_2024/macau_chargingcable.jpg)

At this time of the year it was comfortably cold and pleasant in Beijing and Shanghai. Macau and Hong Kong was humid in comparison, and it was raining all morning during one of our days in Hong Kong.

As an Indian citizen, I did not have any trouble from anywhere, be it the airport, hotel, restaurant, or shops. Apparently the regular people in China don't give a damn about the problems between India and China. The few people I interacted with were very friendly and helpful.

I don't think I want to do further tours in Asia during the forseeable future. I haven't been to Europe yet, so I'm hoping to go there next, perhaps next year. Not sure if my visas would get approved as they have good reasons to reject the visa application of a single young Indian male, but nevertheless I'll give it a try!
