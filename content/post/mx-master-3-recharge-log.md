---
title: "MX Master 3 Recharge Log"
date: 2022-12-31T17:28:21+05:30
draft: false
---

One of my programmer friend's only regret after buying the MX Master 2 (he saved a few bucks by not going for the MX Master 3 -- that's his choice) was that he didn't buy it sooner. That, and [this teardown article](https://blog.bolt.io/logitech-mx-master-3-vs-2s/) about the amount of engineering that went to it made me buy a Logitech MX Master 3 in August 2021.

Praises have been sung for this fantastic piece of engineering over the years, so I'll keep that to a minimum.

My favorite feature of the mouse is the scroll-wheel which lets you to scroll hundreds of lines in a second while also allowing you to slowly scroll line by line, depending on how you flick the wheel. The horizontal-scrolling and the device switching feature are handy.

This mouse works well on Linux and Mac via Bluetooth, but unfortunately [Logitech Options](https://www.logitech.com/en-in/software/options.html) app isn't supported on Linux (and I haven't bothered to install it on my work Macbook). Hence I have not made any customizations to this mouse. I'm aware that there's a third party app for Linux but I haven't tried it.

My use-case is lots of coding (which involves scrolling large codebases), lots of internet browsing, and no gaming.

Here's my recharge frequency logs so far:

|#| Recharge date      | Days since last charge|Device          | Notes     |
|---:|-----------------|----------------------:|----------------|-----------|
|   1|Late August 2021    | - |old Dell laptop |Used mostly via the USB IR thing |
|   2|Mid October 2021    | - |"               |"          |
|   3|Early December 2021 | - |"               |"          |
|   4|Mid February 2022   | - |Macbook #1      |Started using Bluetooth|
|   5|22 Mar 2022   | - |"               |Partial    |
|   6|25 Apr 2022   | 34 | "               |-          |
|   7|17 May 2022   | 22 | "               |-          |
|   8|13 Jun 2022   | 27 | "               |Partial    |
|   9|18 Jun 2022   | 5  | ThinkPad        |-          |
|  10|25 Jul 2022   | 37 | Macbook #1      |-          |
|  11|17 Aug 2022   | 23 | Macbook #1      |-          |
|  12|10 Sep 2022   | 24 | ThinkPad        |-          |
|  13|29 Sep 2022   | 19 | Macbook #1      |-          |
|  14|20 Oct 2022   | 21 | Macbook #2      |-          |
|  15|08 Nov 2022   | 19 | Macbook #2      |-          |
|  16|27 Nov 2022   | 19 | ThinkPad        |-          |
|  17|12 Dec 2022   | 15 | Macbook #2      |-          |
|  18|28 Dec 2022   | 16 | Macbook #2      |-          |
|  19|29 Jan 2023   | 32 | ThinkPad        |-          |
|  20|10 Mar 2023   | 40 | Macbook #2      |-          |
|  21|10 Apr 2023   | 31 | ThinkPad        |Unused for 2 weeks prior to this |
|  22|12 May 2023   | 32 | Macbook #2      |-          |
|  23|29 Jun 2023   | 48 | Macbook #2      |Unused for 4 days prior to this |
|  24|14 Aug 2023   | 46 | Macbook #2      |-          |
|  25|29 Sep 2023   | 46 | Macbook #2      |-          |
|  26|02 Nov 2023   | 34 | Macbook #2      |-          |
|  27|17 Dec 2023   | 45 | ThinkPad        |Used only for 2 days per week |

Both Macbook #1 and #2 are Macbook Pro models with Intel chips (irrelevant detail), and Macbook #2 is slightly newer.

![Plot](/images/mouse_recharge/plot.png)

General observations around recharges:
* The number of days that the charge holds for had gradually reduced in 2022, from about 45 days to less than 21 days on average. However, since 2023 the number has climbed and settled into an average of 41 days. I do not know why it dipped earlier or why it has increased now.
* I recharge the mouse when the red indicator lights up. And I always recharge it using the supplied USB cable connected to my laptop; in the case of the Macbooks I use an adapter (which also supplies the HDMI signals to my monitor). Takes about 3 hours to recharge.
* The drop in charge-holding (don't know if that's the proper term) was dramatic after I switched to bluetooth. When I used to use the IR receiver alone, I could go for about 60 days between recharges; with bluetooth this figure has decreased to about 45 days.
* The mouse is used every weekday during working hours, and perhaps in the evening or during the weekends. I always switch off (not just sleep) my laptop after the day's work; hence it shouldn't be talking to the mouse at night. I don't always switch off the mouse every evening however, but I will if I know I'm unlikely to need my mouse in the coming days. This usually happens on weekends or when I go on a holiday.
* The charge drops significantly if I don't use the mouse for several days in a row. On more than one occasion after keeping the mouse switched off for a few days it was completely dead. As mentioned previously, I do switch off the mouse when I know beforehand that it will be unused for several days.

Just for comparison -- and I'm aware this is not an apples to apples comparison (pun not intended) -- my wireless Apple keyboard holds its charge for like 8 months.

Despite it not holding its charge as much as I'd like it to, I'm overall happy with the mouse.

I intend to keep this post updated with the recharge logs and when anything noteworthy happens.

-----------------

As an aside, here's the quick-and-dirty Python script that I'm using to compute the intervals between dates:

{{< highlight python "linenos=table" >}}
import datetime

dates = [
    '22 Mar 2022',
    '25 Apr 2022',
    '17 May 2022',
]

prev = None
for datestring in dates:
    d = datetime.datetime.strptime(datestring, '%d %b %Y').date()
    if prev:
        print(prev, d, (d-prev).days)
    prev = d
{{< / highlight >}}