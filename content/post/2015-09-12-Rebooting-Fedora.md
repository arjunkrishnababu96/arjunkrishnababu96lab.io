---
layout: post
title: Rebooting Fedora!
date: 2015-09-12
---
Remember when I told you about my brief experience with the Fedora? Or perhaps you don't, coz I had written about that in my [older blog](https://classiccoder.wordpress.com/2015/06/06/ubuntu-to-fedora-and-back-to-ubuntu/).
Anyways, to put a long story short, I had switched from a solid Ubuntu installation to Fedora 22, only to switch back to my good old Trusty Tahr in less than 72 hours!

Fedora was just too bleeding edge for me!

However, I'm proud to say that I've switched back to Fedora! Or to be more precise, I've dual-booted my laptop with Ubuntu and Fedora.
The reasons for it are multifold; one thing which I did notice during my one-night stand with this bleeding edge beauty is its performance. A very noticeable bump from what already seemed like snappy performance in Ubuntu (the difference became even more evident when I switched back to Ubuntu from Fedora).

And secondly, this time around I intend to contribute to the Fedora project; Fedora is one thing which could really use a lot of improvement, and I intend to help with that process to the best of my abilities. Besides, it would give a boost to my resume; really understood the importance of building up the resume only when I submitted my application to the student-exchange program.

Last, but not the least, by dual-booting my machine with Ubuntu and Fedora, I get the best of both worlds; a much better platform for getting used to using Fedora while still having the humaneness of Ubuntu. This way I can gradually get used to using Fedora full-time, like I always wanted to.

I was initially apprehensive about the dual-booting process itself, but as it turned out, the only hitch I did have is that it took over two full hours to shrink my existing partition to make space for the new OS.

Shrinking was done using a live image of Ubuntu, which comes pre-installed with gparted. Althrough the actual shriking process was a straightforward task, it took rather long to complete on my system.

Once the shrinking process of the Ubuntu partition was over, I used a live image of Fedora to install it into the newly freed space in my hard-disk.

Anyway, the important thing to remember while partitioning your disk is that never alter the starting sector of a partition while extending or shrinking it. You should be able to avoid most problems if you follow this simple rule of thumb.

For more instructions regarding the partitioning of disks, go [here](https://www.maketecheasier.com/dual-boot-ubuntu-fedora/).

Looking forward to a great and fulfilling experience with Fedora!