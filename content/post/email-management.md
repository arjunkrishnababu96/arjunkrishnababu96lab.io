---
title: "Email Management"
date: 2023-09-09T10:01:13+05:30
draft: false
---

I use GMail for most (not all) of my email needs. I'm aware of the risks of getting spontaneously banned from it, but that's not what I wanted to talk about today.

In this post I'll talk about how I have automated the handling of non-personal emails (ie., emails not sent by a human to me, such as newsletters, updates from various services etc).

Initially I used to manually label the emails to make it easier to deal with later. After a while I started liberally using the mail-filter feature to automatically label the emails based on who it's from and the keywords within the message. This removed the need for me to manually label the emails myself, and my inbox stayed a little more organized.

However, deleting emails were complicated. Plenty of non-personal emails are relevant to me when I receive them but a week later it may no longer be relevant, at which point I want them deleted. Newsletters, update-emails from most web services, emails from delivery services et cetera fall into this category. For example, I might want to keep the order confirmation email from Amazon when I place an order, but a week after I receive the package I no longer want to have that email lying around. But by then that email would be buried beneath a 100 other emails, and I might simply forget about it.

My general strategy for deleting old emails was to automatically label them "`ToDelete`" via filters based on whether they satisfied certain criteria, and once in a while I manually bulk-deleted those emails. I stuck with this strategy for years. But over time, some slip through and eventually I would have an inbox with plenty of unwanted emails.

GMail filters do not give me the ability to delete an email after a while. I could have written a script to process emails based on various rules, but I didn't want to set up a private server just for this.

Eventually, I stumbled on this GSuite feature called "Apps Scripts" that lets you write scripts that interact with your documents, sheets, emails etc. The scripting language is similar to JavaScript.

I wrote an app-script to delete emails after a delay. The way it works is this:
* Using the filters feature, most emails are labelled as `DeleteSoon`, `DeleteLater`, or `DeleteEventually`. 
* My app-script would run every week and applies the following rule:
    * If an email labelled `DeleteSoon` is older than 7 days, it gets deleted.
    * If an email labelled `DeleteLater` is older than 14 days, it gets deleted.
    * If an email labelled `DeleteEventually` is older than 30 days, it gets deleted.

Here's the script:
{{< highlight javascript "linenos=table" >}}
const MILLISECONDS_PER_DAY = 24 * 60 * 60 * 1000;
const NOW = Date.now();

const LABEL_DELETE_SOON = {
  name: 'ToRemove/DeleteSoon',
  older_than: new Date(NOW - 7 * MILLISECONDS_PER_DAY).getTime(), // 7 days
};

const LABEL_DELETE_LATER = {
  name: 'ToRemove/DeleteLater',
  older_than: new Date(NOW - 14 * MILLISECONDS_PER_DAY).getTime(), // 14 days
};

const LABEL_DELETE_EVENTUALLY = {
  name: 'ToRemove/DeleteEventually',
  older_than: new Date(NOW - 30 * MILLISECONDS_PER_DAY).getTime(), // 30 days
};

const LABELS_TO_CONSIDER = [LABEL_DELETE_SOON, LABEL_DELETE_LATER, LABEL_DELETE_EVENTUALLY];

function deleteOldEmails() {
  LABELS_TO_CONSIDER.map(function (label) {
    GmailApp
      .getUserLabelByName(label.name)
      .getThreads()
      .filter((thread) => thread.isInInbox() && (thread.getLastMessageDate().getTime() < label.older_than))
      .map(function (thread) {
        Logger.log('Deleting ' + label.name + ' ' + thread.getLastMessageDate() + ': ' + thread.getFirstMessageSubject());
        thread.moveToTrash();
      });
  })
}
{{< / highlight >}}

It's quick and dirty, and I know the code quality could be improved.

I've set things up to run the `deleteOldEmails()` method once a week. A consequence of running it once a week instead of everyday is that that a `DeleteSoon` labelled email can potentially linger in my inbox for nearly 2 weeks instead of just 7 days (and likewise for emails with the other labels). However, I'd rather be on the conservative side so that I have a longer duration to handle them manually.

This has been running flawlessly in "production" for almost a year now.

I still handle the deletion of most emails when I read it, and sometimes instead of deleting the emails I just mark them as, say, `DeleteLater` (unless it got marked automatically based on a filter), but my inbox no longer goes completely out of control when I neglect it for a while. It's a good safety net.

I'm aware that this locks me into using GMail but I'm not too bothered. If I eventually decide to move to a different provider, I'll probably write my own script to handle email deletion and host it somewhere, or run it straight from my computer.

Some useful links:
* [App Scripts homepage.](https://developers.google.com/apps-script)
* [Documentation on how to work with GmailApp in Apps Script.](https://developers.google.com/apps-script/reference/gmail/gmail-app)

*Wondering how long before somebody gets the bright idea to put this feature behind a paywall.*
