---
title: "In Japan"
date: 2023-03-28T11:08:53+05:30
draft: false
---

![](/images/japan_mar_apr_2023/nrt.jpg)

Just landed at Narita International Airport in Tokyo, Japan :jp:.

This is a family holiday trip. I'll spend nearly two weeks here. In addition to Tokyo, I'll also visit Mt. Fuji :mount_fuji: , Hiroshima, Osaka, Himeji, Kyoto, and Nara.

Japan wasn't ever in my bucket list so I'm pleasantly amused to find myself here. Looking forward to viewing the cherry blossoms :cherry_blossom:. 
