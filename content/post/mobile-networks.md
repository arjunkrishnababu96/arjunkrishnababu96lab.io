---
title: "Things You Should Know About Mobile Networks"
date: 2018-09-07T20:03:59-06:00
draft: false
---
Here are some things about mobile networks that I learnt the hard way.

While I was in India during the summer, I purchased the [Nokia 6.1](https://www.nokia.com/en_int/phones/nokia-6)* smartphone. It is an [Android One](https://www.android.com/one/) device.

In India, your phone and your carrier are two separate independent things. Unlocked phones are the default there. That means, to change your phone, get yourself a new phone and put in your old SIM card. To change your SIM card, get a new SIM card and put it in your old phone. Trivial, right?!

The Nokia 6.1 is a dual-sim device, and I inserted both my Indian and US SIM card. I was connected to both networks (my US SIM has international roaming).

A months later, I was back in the U.S. I landed in Chicago, turned on my phone, and it wasn't picking up the network at all. It showed "No service". I was able to connect to WiFi, so I did have some sort of connectivity. But not having a working phone was annoying.

It can't be anything wrong with my phone as it was working perfectly fine in India, even with the US SIM. And this phone is a model that is on sale in the US. So it's not like I bought something from an obscure brand. Either way, I tried online solutions in vain, such as:

* Rebooting the device
* Getting in and out of flight mode
* Factory-reset my network settings
* Fiddling with radio settings using special codes -- the really dark art of smartphone troubleshooting!

After some back and forth with my carrier, they sent me a replacement SIM. That didn't work either. Frustrated, I decided to get a new SIM from a different carrier. They tried on one of their SIM to make sure that it would work, but they couldn't get it to work! It was frustrating!

I began my own investigation into this. I study computer science after all. 

Turns out different countries use different GSM and LTE frequencies to connect to the network!

Quoting from the [Wikipedia article on GSM Frequencies](https://en.wikipedia.org/wiki/GSM_frequency_bands#GSM_frequency_usage_around_the_world):<br>

"**GSM-900 and GSM-1800 are used in most parts of the world** (ITU-Regions 1 and 3): Africa, Europe, Middle East, Asia (apart from Japan and South Korea where GSM has never been introduced) and Oceania.

**GSM-1900 and GSM-850 are used in most of North, South and Central America** (ITU-Region 2). In North America, GSM operates on the primary mobile communication bands 850 MHz and 1900 MHz."

Long story short -- Americas use GSM-850 and GSM-1900, and nearly all other country uses GSM-900 and GSM-1800!

But then, why was I able to use my previous phone -- an Asus Zenfone -- in both India and USA (and other countries as well)? Turns out, most phones support all of these frequencies. Such phones are called "[tri-band](https://www.phonescoop.com/glossary/term.php?gid=136)" or "[quad band](https://www.phonescoop.com/glossary/term.php?gid=139)" phones, a.k.a "world phones". Very useful if you travel a lot internationally. My particular Nokia 6.1, however, was a version called TA-1089 which is a [dual-band](https://www.phonescoop.com/glossary/term.php?gid=135) version, and it only supports GSM-900 and GSM-1800!

I accepted the fact that my phone would not work in the US.

My only option now was to get myself a new phone. And that's exactly what I did. I bought another Nokia 6.1, and this time it's a version that supports both the frequencies in the US and well as India (TA-1045, and it works pretty much everywhere, for that matter).

I bought the same model because I didn't want to waste days looking over phone reviews. The Nokia, apart from the frequency issue, is an otherwise fine piece of engineering and serves me well.

And yes, the phone arrived, I put in my SIM card, and it connected to the network! Yay!

---

*\* I believe the Nokia 6.1 is also known as Nokia 6 2018.*
