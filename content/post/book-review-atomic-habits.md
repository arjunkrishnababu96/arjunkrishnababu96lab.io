---
title: "Book Review: Atomic Habits"
date: 2021-05-29T22:15:25+05:30
draft: false
book:
    title: 'Atomic Habits'
    author: 'James Clear'
---

I normally shy away from self-help books because they rarely work for me, but I decided to give Atomic Habits by James Clear a shot because it was recommended to me over a year ago by an esteemed colleague of mine, and he was a living example of how the ideas from this book transformed his life.

The first 50 pages present the core idea of Atomic Habits -- our habits and small changes (however insignificant they may be) can have a profound impact on our lives. The next 150+ pages talk about ways to create and stick with habits.

Don't get me wrong -- there is no groundbreaking magic in here. This book says out explicitly what I suspect most of us know intuitively. There are, however, very actionable steps that can help us get going in the right direction, and for that the book is worth its money.

The book also mentions plenty of examples of people who natually ended up doing what the author advocates, and got good results from it. However some of those examples felt as through the author twisted the situation to make it align with what he's trying to say.

The book is well organized -- several sections with each chapter within a section presenting a concrete idea. Pacing is good. However, all said and done, it could have been a lot more concise.

Do I recommend this book? Yes, but I'll also add that I'm glad I waited until prices dropped on Amazon. It's one of the better self-help books around and is certainly not one of those books whose ideas evaporate as soon as you're done with it.