---
layout: post
title: At the Google Play App Excellence Summit
date: 2017-08-04
excerpt: Attended the Google Play App Excellence Summit in Bengaluru, India.
tags: tag1, tag2
---
[Google Play App Excellence Summit](https://events.withgoogle.com/made-for-india/) was held on August 1st at Bengaluru, India. Entry to the program was free if you were invited (only invitees were allowed to attend).

I arrived at the venue -- [Sheraton Grand Hotel](http://www.sheratongrandbangalore.com/) -- by 8:30 am. The place was already crowded with enthusiastic developers from all over the country despite me arriving an hour ahead of schedule. Breakfast was included; too bad I wasn't very hungry.

While waiting for the program to start, I checked out the [Google Daydream](https://vr.google.com/daydream/) demo nearby. I'm skeptical about whether I would ever have a good reason to use the technology on a daily basis. In my opinion, VR is still in its infancy. It remains to be seen if people come up with interesting applications that use this technology.

The welcome message by Purnima Kochikar was amusing. She threw us a lot of facts and statistics. The gist of it was that India now has more smartphone users than the United States (Should that be surprising? After all, India has 4 times the population of the United States). And the number of users are set to rapidly increase over the next several years.

The talking point is that India's new smartphone users -- the ones that come after the first 300 million smartphone users -- are unfamiliar with smartphones, computers or the internet. These users are accessing the internet for the first time though their smartphones. We developers have the opportunity to build apps that address the needs of these new users while also solving the unique challenges our country faces. And perhaps even make money in the process.

The internet infrastructure here in India isn't as reliable as those in developed economies like the United States. For instance:

* 3G connectivity became common only recently. 4G is still a relatively new thing here.
* Wireless internet connectivity is sketchy at most places.
* People are conscious about the amount of bandwidth they use.
* New adopters of smartphones tend to purchase low-end devices with low memory, processing power, and storage space.

Those shiny apps that work like a charm in the west would simply stutter and lag here in India. This summit was aimed at providing tips to developers to ensure that our apps work well even on low-end smartphones with limited internet connectivity.

To cite an example, consider [YouTube Go](https://youtubego.com), a lightweight version of the popular YouTube app optimized for low-end smartphones and sketchy network connectivity. Quoting from their [official website](https://youtubego.com), YouTube Go allows you to:

* Download videos and watch them later (this way you avoid buffering when you have a slow connection).
* Preview the videos before you watch or download them.
* Know the exact amount of memory you need to watch or download a video.
* Share videos to friends without using the intenet (through an ad hoc wifi network I guess).

One session that I particularly enjoyed was the panel discussion with top level management of [Times Internet](http://timesinternet.in/), [MakeMyTrip](https://www.makemytrip.com/), [Moonfrog](http://moonfroglabs.com/), and [Byju's](https://byjus.com/), about how they optimized their apps for emerging markets like India. Provided a lot of insights.

You can find the video of the whole session on YouTube.
{{< youtube 60mdDRk-x-M >}}

It was a day well spent, and I came back home with a lot more knowledge than I had in the morning.