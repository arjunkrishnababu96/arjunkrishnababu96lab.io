---
title: "Mozilla 101"
date: 2018-07-17T22:04:11+05:30
draft: false
---

It's been a while since I've started contributing code to the Mozilla codebase.

Because I frequently nuke and re-install everything on my primary computer (due to other experiments that have nothing to do with Mozilla), I've decided to write myself a reference I can quickly refer to when setting up the local version of Firefox. All of the following were performed on Debian-based distros.

I plan to make this a living document; expect changes here as I learn more.

**None of this is official. I'm just documenting what worked for me.**

---

#### 1. Firefox

##### 1.1 Initial Build

Build instructions on Linux machines are [here.](https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Build_Instructions/Simple_Firefox_build/Linux_and_MacOS_build_preparation) It goes something like this:
```
mkdir src && cd src
wget https://hg.mozilla.org/mozilla-central/raw-file/default/python/mozboot/bin/bootstrap.py
python2 bootstrap.py
```

Apparently, `bootstrap.py` only works with python 2 (the last time I checked). On my machines, `python` defaults to python 3, and hence the `python2` above to be on the safer side. Follow the prompts and stick with the defaults. Accept anything that claims to make your life easier (such as the several widgets and tools). Whatever you do, ensure that [mozreview](https://mozilla-version-control-tools.readthedocs.io/en/latest/mozreview.html) is set up correctly.

I normally don't allow bootstrap.py to clone `mozilla-unified`, and instead clone `mozilla-central` repository.
```
hg clone https://hg.mozilla.org/mozilla-central
```

You now have all the code available locally. Now it's time to build Firefox:
```
cd mozilla-central
./mach build
./mach run
```
The build can take a while. The last command above runs firefox once the build is complete.

##### 1.2 Updating Local Copy
Must do this when you're about to start working on a new bug.
```
hg pull
hg update central
./mach build
```
While attempting to build, it sometimes complains that a clobber is required, along with instructions on how to do that. Follow the instructions. If clobber is not required, then builds tend to be faster.


##### 1.3 Submitting Code For Review
(These are simple instructions for those who have already read the [official documentation](https://moz-conduit.readthedocs.io/en/latest/phabricator-user.html#user-guide)).

Mozilla has moved away from `mozreview` in the recent past. The recommended way for code review is now via Phabricator. 

Before you do anything, ensure you have an account with [Mozilla's Phabricator instance.](https://phabricator.services.mozilla.com/)

You need to install:

1. Arcanist -- installation instructions are in the [Arcanist Quick Start guide](https://phabricator.services.mozilla.com/book/phabricator/article/arcanist_quick_start/) and in [here.](https://moz-conduit.readthedocs.io/en/latest/phabricator-user.html#setting-up-arcanist) Required for moz-phab (next step).
2. moz-phab
    * Download the python source code for [latest release from its official Github repo.](https://github.com/mozilla-conduit/review/releases/latest/)
    * Make that file executable (`chmod u+x moz-phab` from its containing directory)
    * Add the containing directory to `$PATH`

Once all that is set up, to submit a commit for review, do
```
moz-phab submit
```
`moz-phab` should do the rest; you might want to verify that your submission appears in Phabricator.

##### Useful Links
* [Codetribute](https://codetribute.mozilla.org/)
* [Bugzilla](https://bugzilla.mozilla.org/)
* [Mozilla Phabricator User Guide](https://moz-conduit.readthedocs.io/en/latest/phabricator-user.html)
* [MDN: How to submit a patch](https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/How_to_Submit_a_Patch)


---

#### 2. Taskcluster

Components like [Taskcluster](https://tools.taskcluster.net/) don't come along with Firefox. This should have been obvious, but it took me an hour to get this.

Taskcluster is hosted on GitHub (link below), and according to [this official page](https://docs.taskcluster.net/docs/tutorial/hack-tc), fork and clone the necessary repo to hack on Taskcluster (and make a pull-request once you're done). For instance to work on [taskcluster-queue](https://github.com/taskcluster/taskcluster-queue):

```
git clone https://github.com/taskcluster/taskcluster-queue.git
```

You need to install [yarn](https://yarnpkg.com/en/) before you can, say, run tests on certain files. The [official instructions for installing yarn](https://yarnpkg.com/en/docs/install#debian-stable) goes something like the follows:

```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
```

To install the dependencies in a project (such as [eslint](https://eslint.org/) maybe):
```
yarn install
```

To run all tests:
```
yarn test
```

To run a specific test:
```
$(npm bin)/mocha ./path/to/file.js
```

Some test results might show its results as "Pending". [According to Dustin J. Mitchell](https://bugzilla.mozilla.org/show_bug.cgi?id=1453714#c9), "'Pending' means that the tests are skipped because the necessary credentials are not available."

There are also some [things involving credentials](https://bugzilla.mozilla.org/show_bug.cgi?id=1453714#c9) you need to be aware of. To be honest I don't fully understand this bit yet. And for certain features you need to use [Taskcluster CLI](https://github.com/taskcluster/taskcluster-cli):
```
wget https://index.taskcluster.net/v1/task/project.taskcluster.taskcluster-cli.latest/artifacts/public/linux-amd64/taskcluster
chmod +x taskcluster
./taskcluster help
```
You can replace "help" above with other commands, such as "signin".

##### Useful Links
* https://tools.taskcluster.net/
* https://docs.taskcluster.net/docs/tutorial/hack-tc
* [Taskcluster on GitHub](https://github.com/taskcluster)
* IRC channel #taskcluster
