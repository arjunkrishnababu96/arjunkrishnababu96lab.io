---
layout: post
title: Introducing ShScript Creator
date: 2015-08-10
---

This one is a tribute to all the shell programmers out there.

Of late, I've been having to deal with a lot of shell-scripts, mostly as part of my academic pursuits. And it's fun you know, when you really get to know the power of shells..!

However, despite all the fun, I was somehow irritated at having to write down the hashbang everytime I create a new script. And on top of that, I would also have to chmod the file to give it the execute permissions. Blame it on the fact that I mostly use IDEs like NetBeans and Android Studio among others that do half the work for you, like adding in all the placeholders, parentheses, imports etc. Frankly speaking, having to write the hashbang everytime I create the shell script was kind of annoying.

And that's what prompted me to write a script that would automatically add the hashbang and change the permissions of the file during creation. For about 24 hours I was using it without issues, and it kind of hit upon me to share the script with the others; I can't be the only one on the planet who finds having to write the hashbang everytime I create a shell-script to be annoying.

And to relieve the prospective users of the intricacies involved in installing the file - you'll have to add the location of the script to the PATH variable (so that you can use it from any directory), and also add an alias to the script to get rid of the "./" - I made another script that would install the whole thing onto your linux system.

Once you install the script onto your system, all you need to do to create a new shell-script is to type onto your shell:
```
$ shscript filename.sh
```

And you'd get a new file with the specified name opened, complete with the hashbang and the necessary file permissions! And no, on subsequent opening of the file using `shscript`, the existing contents don't get erased.

The code has been hosted on [my GitHub account](https://github.com/arjunkrishnababu96/ShScriptCreator). For installing the latest release (currently at v0.1.0), go to https://github.com/arjunkrishnababu96/ShScriptCreator/releases

The script isn't quite perfect. For instance, it only works on systems where bash or zsh is the default shell.

And then, the code isn't quite robust. There are some "rare" instances when the code would break. And as of now, the scripts that you open using shscript only opens up in vim, and incase vim isn't installed, you're at a dead end.

However, I would be fixing up all the above mentioned issues in the coming days, including adding support for more shells as well as give users more options regarding the choice of their text-editor.
