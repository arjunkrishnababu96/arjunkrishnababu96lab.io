---
title: "Open Source: Contributing Back to the Community"
date: 2018-11-09T10:33:00-07:00
draft: false
---
It's no secret to people who know me personally that I'm an open-source enthusiast. However, I haven't been contributing back to the community as much as I should have. 

Till now, my focus has been on fixing bugs in existing programs. It all started when I tried [contributing to the Linux kernel](https://kernelnewbies.org/FirstKernelPatch) several years ago. In retrospect, I was biting way more than I could chew. It took me several days of carefully reading through the instructions and code before I was able to submit my first patch. Imagine my thrill when I received an email from [Greg Kroah-Hartman](https://en.wikipedia.org/wiki/Greg_Kroah-Hartman) himself saying that my patch has been accepted!

I contributed a few more patches for a while. Linux kernel contribution was a good learning experience. I became substantially more familiar and comfortable using the terminal and git, learnt to make patches, and send them via email through the terminal. I know, these are probably kids' stuff, but all that was exciting to me back then!

Unfortunately, though I really wanted to continue contributing to the linux kernel I wasn't sure if that would be such a good idea because:

* Setting up my environment involved trashing my OS multiple times. The computer I worked on was my main laptop, where I do my other stuff as well. And I didn't want to use a VM as that might reduce performance.
* There was this one time I screwed up so bad, and the BIOS (I assume) couldn't find my OS. Had to start over. Luckily I had backups -- I had anticipated this sort of thing :) -- and no permanent damages were done.
* The bugs with the linux kernel were way more than what I could handle back then. 
* Even if I fix those bugs, the changes I made weren't immediately visible to me.

So I decided to jump ship and move to a project that didn't involve breaking my kernel. I chose Firefox.

[Setting up my firefox build environment](/post/mozilla-101/) was much easier than setting up the same for Linux kernel. For the past one year, I've been contributing occasional bug-fixes whenever I had free time. Most bugs I have worked on till now were beginner-friendly simple stuffs, so I have way more to go. The changes I made were things I could actually see and think *'Oh yeah! I made that change!'*. This was encouraging.

It was around this time that I realized I should probably fix niggling things on software which I personally use, instead of just sticking to fixing bugs. 

[Firefox Focus](https://www.mozilla.org/en-US/firefox/mobile/#focus) browser for Android does not have a button to open a new empty tab. This was annoying to me, and I often wondered why they have designed things this way. I concluded that it was up to me to fix the issue. I did not even know if it was possible for me to fix the issue myself, but I decided to give it a try anyway. I do have some familiarity with Android and Java and have a knack for quickly picking up new languages (like Kotlin).

Setting up the environment took longer than I thought, though it was a straightforward process; cloned [their repo](https://github.com/mozilla-mobile/focus-android), installed Android Studio, set up AVD, and finally spun up Firefox Focus on an emulator.

{{< figure src="/images/ff-newtab/ff-dev.png" caption="Work in progress" width="100%    ">}}

The work (on opening a new empty tab) is currently in progress, and there are tonnes of confusing bits. Looks like I would be spending a lot of time chatting up with the folks on IRC.

I should also probably upgrade/replace my ageing laptop. Though perfectly adequate for my day-to-day tasks at my university, it's starting to show its age when developing *large* applications. For instance, once I build and emulate an Android app, nearly all of my main memory (7.6 gb of a total 7.7 gb) and a significant amount of swap space would be used up. However, I do appreciate that build times are way faster than what it was 3 years ago when I first experimented with Android.

Compiling Firefox for desktop (Linux) also takes forever. I thought the bottleneck was hard-disk and had upgraded to an SSD but that barely made a difference to my 90 minute build time. Since the there is still spare RAM remaining while building, perhaps the processor is the bottleneck? My processor is an *optimized-for-power-consumption* Intel i5 ending in a U; I'm thinking of switching over to a performance-oriented processor sometime in the future when I can afford it. *I'm looking at you System76!*