---
title: "Index"
date: 2018-07-04T13:11:05+05:30
draft: false
---
Welcome to my website. Here's a list of things you might be interested in:
1. [Blog posts]({{<ref "/post">}} "Blog posts")
2. [About Me]({{<ref "/about">}} "About the author")